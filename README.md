# MegaNotes

**Inicio do sistema**
<p align="center">
  <img src="https://gitlab.com/duzeru/meganotes/raw/master/imagens/01-inicio.png" alt="MegaNotes" width="500">
</p>

**Adicionando tags**
<p align="center">
  <img src="https://gitlab.com/duzeru/meganotes/raw/master/imagens/02-tags.png" alt="MegaNotes" width="500">
</p>

**Alterando múltiplas notas**
<p align="center">
  <img src="https://gitlab.com/duzeru/meganotes/raw/master/imagens/03-notes.png" alt="MegaNotes" width="500">
</p>

A equipe DuZeru lhe proporciona este app robusto, utilizando o Mega Notes pode trazer inúmeras possibilidades. Você pode utilizar para criar notas, cronograma de um projeto, lembretes, até mesmo criar uma agenda.

Algumas notas do tutorial foram adicionadas ao seu diretório de dados. Eles irão guiá-lo para os principais recursos que o MegaNote oferece.

Depois de explorar, sinta-se à vontade para excluí-los permanentemente. Para ajudar no desenvolvimento acesse: 
https://gitlab.com/duzeru


# 01 - O Diretório de Dados
O diretório de dados é onde todas as suas anotações e anexos, tem a seguinte estrutura (por padrão):

```
/home/$USER/.meganote/
├─┬ attachments
│ ├── foo.ext
│ ├── bar.ext
│ └── …
└─┬ notes
  ├── foo.md
  ├── bar.md
  └── …
```

# 02 - A barra lateral

A barra lateral é onde todas as suas anotações são categorizadas.

## Criando Categorias
Para criar uma categoria é simples, apenas clique no campo <img src="@attachment/tag.png"> `editar tags`

## Categorias

- **Todas as notas**: Esta sessão contém todas as notas.
- **Favorites**: Esta sessão contém todas notas favoritas, após clicar na estrela.
- **Notebooks**: Esta seção contém todas as notas marcadas com a tag `Notebooks/*`.
- **Tags**: Esta seção contém todas as notas marcadas com qualquer tag, exceto as especiais: `Notebooks/*` ou `Templates/*`.
- **Templates**: Esta seção contém todas as notas marcadas com a tag `Templates/*`  Estas notas não serão exibidas em nenhuma outra categoria.
- **Untagged**: Esta seção contém todas as notas que não possuem tags.
- **Trash**: Esta seção contém todas as notas que foram excluídas. Estas notas não serão exibidas em nenhuma outra categoria.

Você pode criar subcategorias nas seguintes seções: Notebooks, Tags e Templates usando tags aninhadas.

# 03 - A barra do meio

A barra do meio (intermediária) mostra todas as notas contidas na categoria ativa no momento, ordenadas e filtradas corretamente pela consulta de pesquisa.

## Pesquisar notas

Para pesquisar basta digitar algo na barra de pesquisa.

## Nova nota

Ao lado da barra de pesquisa, há também um botão para criar uma nova nota.

## Ordem de classificação

Logo abaixo da barra de pesquisa, você pode alterar a ordem em que as notas estão sendo exibidas.

## Notas

Por fim, é efetuado a lista de notas para o utilizador.

# 04 - A barra principal

A barra principal é onde você pode visualizar e editar a nota atualmente ativa.

## Barra de ferramentas

A barra de ferramentas contém botões para diversas ações nota atual, todas elas também são acessíveis por meio de atalhos.

## Previa/Edição

Logo abaixo da barra de ferramentas, há a área de visualização/editção

#### Multi-Editção

Quando 2 ou mais notas são selecionadas, um editor de várias notas será exibido na barra principal.

# 06 - Tags

- **Todas as notas**: Aqui encontra-se todas as notas.

-  **Favoritos**: Quando você está na nota e clica na estrela, ela é adicionada a favoritos.

- **Notebooks**: Você provavelmente já percebeu que o MegaNote também suporta notebooks. Para criar um, basta adicionar uma tag na nota começando com `Notebooks/`. 

- **Tags**: Tags não inclusas em notebooks, Templates ou Lixeira.

 **Untagged**: Notas sem Tags.

- **Templates**: O MegaNote também suporta Templates, para criar um, basta adicionar a tag `Templates` a uma nota. Utilize também o aninhamento, por exemplo, `Templates/Teste`.

- **Lixeira** Inclui notas excluídas, até você não excluir permanentemente.

## Collapse/Expand

Tags com SubTags podem ser recolhidas ou expandidas, basta clicar com o botão direito sobre elas e selecionar a opção.

# 07 - Atalhos

<p align="center">
  <img src="@attachment/icon.png" width="192">
</p>

- <kbd>Ctrl+N</kbd> - Novo.
- <kbd>Ctrl+Shift+N</kbd> - Duplicar.
- <kbd>Ctrl+O</kbd> - Abrir no aplicativo padrão.
- <kbd>Ctrl+Alt+R</kbd> - Revelar no Diretorio.
- <kbd>Ctrl+E</kbd> - Alternar edição.
- <kbd>Ctrl+Shift+E</kbd> - Alternar edição.
- <kbd>Ctrl+Shift+P</kbd> - Alternar edição.
- <kbd>Ctrl+Shift+P</kbd> - Alternar edição.
- <kbd>Ctrl+Shift+T</kbd> - Alternar edição de tags.
- <kbd>Ctrl+Shift+A</kbd> - Toggle attachments editing.
- <kbd>Ctrl+D</kbd> - Alternar favorito.
- <kbd>Ctrl+P</kbd> - Alternar pin.
- <kbd>Ctrl+Backspace</kbd> - Mover para o lixo, ao pré-visualizar.
- <kbd>Ctrl+Alt+Backspace</kbd> - Mover para o lixo, ao editar.
- <kbd>Ctrl+Shift+Backspace</kbd> - Restaurar do lixo.

## Edição

- <kbd>Tab</kbd> - Vinculando Anexos/Anotações/Tags.
- <kbd>Shift+Tab</kbd> - Linha atual desatualizada.
- <kbd>Ctrl+Up</kbd> - Mova a linha atual para cima.
- <kbd>Ctrl+Down</kbd> - Mova a linha atual para baixo.
- <kbd>Alt+Click</kbd> - Adicionar um cursor.
- <kbd>Alt-Z</kbd> - Alternar quebra de linha.
- <kbd>Ctrl+Enter</kbd> - Alternar a caixa de um item.
- <kbd>Alt+D</kbd> - Alternar a marca de seleção de um item.

## Multi-Edição

- <kbd>Ctrl+Alt+A</kbd> - Selecionar todas as notas.
- <kbd>Ctrl+Alt+I</kbd> - Inverter notas selecionadas.
- <kbd>Ctrl+Alt+C</kbd> - Limpar notas selecionadas.

## Navegação

- <kbd>Ctrl+Alt+Shift+Tab</kbd> - Tag anterior.
- <kbd>Ctrl+Alt+Tab</kbd> - Próxima tag.
- <kbd>Up</kbd> - Nota anterior, ao visualizar.
- <kbd>Left</kbd> - Nota anterior, ao visualizar.
- <kbd>Ctrl+Shift+Tab</kbd> - Nota anterior.
- <kbd>Down</kbd> - Próxima nota, ao visualizar.
- <kbd>Right</kbd> - Próxima nota, ao visualizar.
- <kbd>Ctrl+Tab</kbd> - Próxima nota.

## Outros

- <kbd>Ctrl+S</kbd> - Alternar para o modo de visualização, ao editar.
- <kbd>Esc</kbd> - Feche o multi-editor ou alterne para o modo de visualização.
- <kbd>Ctrl+F</kbd> - Foco para barra de pesquisar.
- <kbd>Ctrl+Alt+F</kbd> - Alternar modo de foco.


## Contribuição

Para compilar o pacote fonte, baixe-o e execute os comandos abaixo:

```bash
cd MegaNotes
npm install
npm run svelto:dev
npm run iconfont
npm run tutorial
npm run dev
```
